<?php

namespace Experiment\NewSystem\Entity\Domain;

use DomainException;

final class Entity
{
    private $id;
    private $name;

    public function __construct(
        int $id,
        string $name
    ) {
        $this->id = $id;
        $this->name = $name;
    }

    public static function fromPersistence(array $row): self
    {
        return new self(
            (int) $row["ID"],
            $row["NAME"],
        );
    }

    public static function create(
        int $id,
        string $name
    ): self {
        $figure = new self(
            $id,
            $name
        );

        if (strlen($name) < 3) {
            throw new DomainException("Entity's name must have at least 3 characters!");
        }

        return $figure;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function updateTitle(string $name): void
    {
        if (strlen($name) < 3) {
            throw new DomainException("Entity's name must have at least 3 characters!");
        }

        $this->name = $name;
    }
}
