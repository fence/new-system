<?php

declare(strict_types=1);

namespace Experiment\NewSystem\Entity\Application\UpdateEntity;

final class UpdateEntityCommand
{
    private $id;
    private $agentId;

    public function __construct(
        int $id,
        int $agentId
    ) {
        $this->id = $id;
        $this->agentId = $agentId;
    }

    public static function fromHttpRequest(array $input, $agentId)
    {
        $command = new self(
            (int) $input["id"],
            $agentId
        );

        if (array_key_exists("name", $input)) {
            $command->name = $input["name"];
        }

        return $command;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function agentId(): int
    {
        return $this->agentId;
    }

    public function name(): string
    {
        return $this->name;
    }
}
