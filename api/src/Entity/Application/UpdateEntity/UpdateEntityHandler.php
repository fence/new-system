<?php

declare(strict_types=1);

namespace Experiment\NewSystem\Entity\Application\UpdateEntity;

use Experiment\NewSystem\Entity\Domain\Entity;
use Experiment\NewSystem\Entity\Domain\EntityRepositoryInterface;
use Experiment\NewSystem\Entity\Domain\EntityReadRepositoryInterface;

final class UpdateEntityHandler
{
    private $entityRepository;

    public function __construct(
        EntityRepositoryInterface $entityRepository,
        EntityReadRepositoryInterface $entityReadRepositoryInterface
    ) {
        $this->entityRepository = $entityRepository;
        $this->entityReadRepositoryInterface = $entityReadRepositoryInterface;
    }

    public function handle(UpdateEntityCommand $command): Entity
    {
        $this->command = $command;
        $entity = $this->entityReadRepository->findById($command->id());

        $entity->updateName($command->name());

        $this->entityRepository->updateEntity($entity, $command->agentId());
        return $entity;
    }
}
