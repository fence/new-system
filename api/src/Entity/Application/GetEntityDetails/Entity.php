<?php

namespace Experiment\NewSystem\Entity\Application\GetEntityDetails;

use JsonSerializable;

final class Entity implements JsonSerializable
{
    private $id;
    private $name;
    private $instituteId;
    private $instituteName;

    public function __construct(
        int $id,
        string $name,
        int $instituteId,
        string $instituteName
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->instituteId = $instituteId;
        $this->instituteName = $instituteName;
    }

    public static function fromPersistence(array $row): self
    {
        return new self(
            (int) $row["ID"],
            $row["NAME"],
            (int) $row["INSTITUTE_ID"],
            $row["INSTITUTE_NAME"]
        );
    }

    public function jsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "institute" => [
                "id" => $this->instituteId,
                "name" => $this->instituteName
            ]
        ];
    }
}
