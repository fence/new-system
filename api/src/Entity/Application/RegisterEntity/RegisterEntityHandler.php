<?php

declare(strict_types=1);

namespace Experiment\NewSystem\Entity\Application\RegisterEntity;

use Experiment\NewSystem\Entity\Domain\Entity;
use Experiment\NewSystem\Entity\Domain\EntityRepositoryInterface;

final class RegisterEntityHandler
{
    private $entityRepository;

    public function __construct(
        EntityRepositoryInterface $entityRepository
    ) {
        $this->entityRepository = $entityRepository;
    }

    public function handle(RegisterEntityCommand $command): Entity
    {
        $entity = Entity::create(
            $this->entityRepository->findNextEntityId(),
            $command->name()
        );
        $this->entityRepository->registerEntity($entity, $command->agentId());
        return $entity;
    }
}
