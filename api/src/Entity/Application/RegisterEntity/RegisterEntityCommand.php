<?php

declare(strict_types=1);

namespace Experiment\NewSystem\Entity\Application\RegisterEntity;

final class RegisterEntityCommand
{
    private $name;
    private $agentId;

    private function __construct(
        string $name,
        int $agentId
    ) {
        $this->name = $name;
        $this->agentId = $agentId;
    }

    public static function fromHttpRequest(array $input, int $agentId): self
    {
        return new self(
            $input["name"],
            $agentId
        );
    }

    public function name(): string
    {
        return $this->name;
    }

    public function agentId(): int
    {
        return $this->agentId;
    }
}
